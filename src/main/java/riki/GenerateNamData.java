package riki;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import io.sigpipe.sing.dataset.Metadata;
import io.sigpipe.sing.dataset.feature.Feature;
import io.sigpipe.sing.dataset.feature.FeatureSet;
import io.sigpipe.sing.serialization.SerializationException;
import io.sigpipe.sing.serialization.SerializationInputStream;
import io.sigpipe.sing.serialization.Serializer;
import io.sigpipe.sing.util.Geohash;

/**
 * THIS READS THE MBLOB FILES AND CONVERTS THEM INTO BLOCKS FOR GALILEO CONSUMPTION
 * 
 * @author sapmitra
 *
 */
public class GenerateNamData {
	
	public static String currentYear = "2013";
	
	/*public static String[] geohashes_2char = {"b0","b1","b2","b3","b4","b5","b6","b7","b8","b9","c0","c1","c2","c3","c4","c5","c6","c7"
			,"c8","c9","bb","bc","bd","be","bf","bg","bh","bk","d0","bn","d1","d2","d3","d4","d5","bs","d6","d7","bu","d8"
			,"d9","bz","cb","cc","cd","ce","cf","cg","ch","ck","cm","e0","e1","cp","e2","cq","e3","cr","cs","e5","e6","ct"
			,"cu","e7","cv","e9","cw","cx","cy","cz","db","dc","dd","de","df","dg","dh","dj","dk","dm","dn","f0","f1","dp"
			,"f2","dq","f3","dr","f4","f5","ds","f6","dt","du","f7","f8","dv","dw","f9","dx","dy","dz","eb","ec","ed","ee"
			,"ef","eh","ej","ek","em","g0","en","g1","ep","g2","eq","g3","g4","er","g5","es","et","g6","g7","eu","ev","g8"
			,"g9","ew","ex","ey","ez","fb","fc","fd","fe","ff","fg","fh","fj","fk","fm","fn","fp","fr","fs","h5","ft","fu"
			,"h7","fv","fw","fx","fz","gb","gc","gd","ge","gf","gg","gh","gk","gn","gp","gq","gr","gs","gt","gu","gv","gw"
			,"gx","gy","hf","hg","j1","j5","j7","j9","hz","k1","k2","k3","k6","k7","k8","k9","jb","jd","je","jg","jp","jw"
			,"jx","kb","kd","ke","kg","kk","km","m0","kn","m2","kp","kq","kr","ks","m5","kt","ku","kv","kw","m9","kx","ky"
			,"kz","n3","n6","mh","mj","mk","mm","mn","mp","mq","mr","mv","mw","mx","my","mz","nd","nh","nk","p4","ns","p7"
			,"p9","q7","q9","pc","pd","pe","pf","pg","ph","r0","r1","r2","r3","r4","r5","r6","r7","pw","px","py","pz","qc"
			,"qd","qe","qf","qg","qj","qm","s0","s1","s2","qp","qq","s3","s4","qr","12","s5","qs","13","14","s6","qt","qu"
			,"s7","qv","s8","qw","s9","qx","qy","qz","rb","rc","rd","re","0c","rh","0f","rj","rk","rm","rn","t0","rp","20"
			,"rq","t4","rr","t5","rs","rt","24","ru","t7","t8","rv","t9","rw","rx","ry","rz","sb","sc","sd","se","sf","sg"
			,"sh","sj","sk","sm","sn","u0","u1","sp","u2","u3","sq","sr","u4","u5","ss","u6","st","u7","su","35","u8","sv"
			,"u9","sw","sx","sy","sz","tb","tc","td","te","tf","tg","2e","th","2g","tj","2h","tk","2j","2k","tm","v0","tn"
			,"v1","2n","v2","tp","tq","v3","v4","2p","tr","v5","ts","2q","tt","v6","tu","v7","2s","v8","tv","2t","tw","v9"
			,"2u","47","tx","2v","ty","tz","2y","ub","uc","ud","ue","uf","ug","3e","uh","uj","uk","um","w0","un","w1","w2"
			,"w3","uq","w4","3p","w5","us","w6","ut","54","w7","uu","w8","56","w9","uw","3u","59","3x","3z","vb","vc","vd"
			,"ve","vf","vg","4e","vh","vk","x0","x1","4m","x2","x3","vq","62","x4","4q","vs","63","x5","4r","64","vt","vu"
			,"4s","x7","x8","66","vv","67","x9","vw","68","69","4w","4x","wb","wc","wd","5b","we","wf","wg","5e","wh","5f"
			,"5g","wj","wk","5j","wm","y0","wn","y1","y2","wp","5n","y3","wq","y4","wr","ws","y5","wt","y6","75","y7","wu"
			,"wv","y8","y9","ww","wx","wy","79","wz","xb","xc","xe","6c","6d","xf","6e","6f","xh","6g","xj","6h","xk","xm"
			,"6k","xn","z0","6m","z1","xp","80","z2","6n","z3","xq","6p","z4","xr","82","83","z5","6q","xs","z6","84","6r"
			,"xt","6s","z7","85","6t","z8","86","xv","z9","6u","xw","87","6v","xx","88","89","xy","6w","xz","6x","6y","6z"
			,"yb","yc","yd","7b","ye","yf","yg","yh","7h","yk","7j","ym","7k","yn","7n","91","yq","7p","93","ys","7q","yt"
			,"94","7r","yu","95","96","yv","7v","7w","yy","7y","7z","zb","zc","zd","8b","8c","ze","zf","8d","zg","8e","8f"
			,"zh","8g","zj","8h","zk","8j","8k","zm","8m","8n","8p","zs","zu","8s","8t","8u","8v","8w","zy","8x","8y","8z"
			,"9b","9d","9e","9f","9g","9h","9j","9m","9n","9p","9q","9r","9s","9t","9u","9v","9w","9x","9y","9z"};*/
	
	//public static List<String> validGeoHashes = new ArrayList<String>(Arrays.asList(geohashes_2char));
	 
	// Features are printed in this order, preceeded by "gps_abs_lat","gps_abs_long","epoch_time",
	public static List<String> desiredfeatures = new ArrayList<String>(Arrays.asList("geopotential_height_lltw","water_equiv_of_accum_snow_depth_surface","drag_coefficient_surface","sensible_heat_net_flux_surface",
			"categorical_ice_pellets_yes1_no0_surface","visibility_surface","number_of_soil_layers_in_root_zone_surface","categorical_freezing_rain_yes1_no0_surface",
			"pressure_reduced_to_msl_msl","upward_short_wave_rad_flux_surface","relative_humidity_zerodegc_isotherm","missing_pblri","categorical_snow_yes1_no0_surface",
			"u-component_of_wind_tropopause","surface_wind_gust_surface","total_cloud_cover_entire_atmosphere","upward_long_wave_rad_flux_surface","land_cover_land1_sea0_surface",
			"vegitation_type_as_in_sib_surface","v-component_of_wind_pblri","convective_precipitation_surface_1_hour_accumulation","albedo_surface","lightning_surface",
			"ice_cover_ice1_no_ice0_surface","convective_inhibition_surface","pressure_surface","transpiration_stress-onset_soil_moisture_surface","soil_porosity_surface",
			"vegetation_surface","categorical_rain_yes1_no0_surface","downward_long_wave_rad_flux_surface","planetary_boundary_layer_height_surface","soil_type_as_in_zobler_surface",
			"geopotential_height_cloud_base","friction_velocity_surface","maximumcomposite_radar_reflectivity_entire_atmosphere","plant_canopy_surface_water_surface",
			"v-component_of_wind_maximum_wind","geopotential_height_zerodegc_isotherm","mean_sea_level_pressure_nam_model_reduction_msl","total_precipitation_surface_1_hour_accumulation",
			"temperature_surface","snow_cover_surface","geopotential_height_surface","convective_available_potential_energy_surface","latent_heat_net_flux_surface",
			"surface_roughness_surface","pressure_maximum_wind","temperature_tropopause","geopotential_height_pblri","pressure_tropopause","snow_depth_surface",
			"v-component_of_wind_tropopause","downward_short_wave_rad_flux_surface","u-component_of_wind_maximum_wind",
			"wilting_point_surface","precipitable_water_entire_atmosphere","u-component_of_wind_pblri","direct_evaporation_cease_soil_moisture_surface"));
	
    
    /**
     * 
     * @author sapmitra
     * @param filePath
     * @param outputPath provided from commandline
     */
    public static boolean readEachFile(String filePath, String outputPath) {
    	
    	try {
    		
    		if(!filePath.endsWith("_001.grb.mblob"))
    			return false;
    		
    		String[] nameTokens = filePath.split("_");
    		
    		String observationDate = nameTokens[2];
    		
    		String year = observationDate.substring(0,4);
    		
    		if(!year.equals(currentYear))
    			return false;
    		
    		String month = observationDate.substring(4,6);
    		String day = observationDate.substring(6,8);
			
			String formatted_date = year+"-"+month+"-"+day;
			//System.out.println(formatted_date);
			
			String observationTime = nameTokens[3];
			
			long timestamp = getTimestamp(year,month, day, observationTime.substring(0,2), observationTime.substring(2,4));
    		
            // path to the mblob file
            SerializationInputStream inputStream = new SerializationInputStream(new FileInputStream(filePath));
            // this is the number of records in a .mblob file
            int recordCount = inputStream.readInt();
            
            // parse each record
            String fileKey = "";
            StringBuffer records = new StringBuffer("");
            
            //System.out.println("READING "+filePath+" "+recordCount+" records");
            
            for (int i = 0; i < recordCount; i++) {
            	/*if(i%100000 == 0)
            		System.out.println(i);*/
            	boolean includeRecord = true;
            	
            	int desiredCount = desiredfeatures.size();
            	
                // lat and lon - geospatial location of the record
                float lat = inputStream.readFloat();
                float lon = inputStream.readFloat();
                
                String geohash = Geohash.encode(lat, lon, 2);
                
                /*if(!validGeoHashes.contains(geohash))
                	continue;*/
                	
                
                String fileName = outputPath+formatted_date+"-"+geohash;
                // fileKey = old key
                if(fileKey != fileName && i!=0) {
                	// write out records
                	writeToFile(records, fileKey);
                	// flush records
                	records = new StringBuffer("");
                	fileKey = fileName;
                } else if(i == 0) {
                	fileKey = fileName;
                }
                
                // payload
                byte[] payload = inputStream.readField();
                
                Metadata eventMetadata = Serializer.deserialize(Metadata.class, payload);
                // temporal properties of the record
                //long startTS = eventMetadata.getTemporalProperties().getStart();
                //long endTS = eventMetadata.getTemporalProperties().getEnd();
                // these are the different feature values encoded in a single record
                
                String rec = lat+","+lon+","+timestamp+",";
                
                FeatureSet features = eventMetadata.getAttributes();
                Iterator<Feature> iterator = features.iterator();
                
                String[] line = new String[desiredfeatures.size()];
                
                // rest of the features
                while (iterator.hasNext()) {
                    Feature f = iterator.next();
                    
                    String featureName = f.getName();
                    int indx = desiredfeatures.indexOf(featureName);
                    if(indx >= 0) {
                    	desiredCount--;
                    	line[indx] = f.getString();
                    }
                    
                }
                
                //System.out.println("MISSING: "+desiredCount);
                // All features have been found
                if(desiredCount < 2) {
                	
                	String featureString = rec;
                	for(int j=0; j< line.length; j++) {
                		if(j == line.length - 1)
                			featureString+=line[j];
                		else
                			featureString+=line[j]+",";
                	}
                	records.append(featureString+"\n");
                }
                
                //System.out.println(rec);
            }
            inputStream.close();
            
            return true;
        } catch (IOException | SerializationException e) {
            e.printStackTrace();
        }
		return false;
    	
    }
    
    private static void writeToFile(StringBuffer records, String fileName) throws FileNotFoundException {
		// TODO Auto-generated method stub
    	
    	File f = new File(fileName);

    	PrintWriter out = null;
    	if ( f.exists() && !f.isDirectory() ) {
    	    out = new PrintWriter(new FileOutputStream(new File(fileName), true));
    	}
    	else {
    	    out = new PrintWriter(fileName);
    	}
    	out.append(records.toString());
    	out.close();
    	
	}

    
	public static void main(String[] args) throws UnknownHostException {
		
		System.out.println("ENTERED");
		// The path with all files in it
		String inputPath = args[0];
		// The path where we want all partitioned files
		//String outputPath = args[1];
		// path to the mblob file
		//String filePath = "/s/green/a/tmp/sapmitra/namanl_218_20150131_1800_001.grb.mblob";
		
		String hostName = InetAddress.getLocalHost().getHostName();
    	String outputPath = "/s/"+hostName+"/a/nobackup/galileo/sapmitra/nam_Ignite_2013/";
		
		File resultsDir = new File(outputPath);
		if (!resultsDir.exists())
			resultsDir.mkdirs();
		
		
		File file = new File(inputPath);
		System.out.println(inputPath);
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			
			int fCount = 0;
			for(File f : files) {
				boolean status  = readEachFile(f.getAbsolutePath(), outputPath);
				
				if(status) {
					fCount++;
					if(fCount % 10 == 0)
						System.out.println("FILES PROCESSED: "+fCount);
				}
			}
		}
		//readEachFile(inputPath);
		
	}
    
    
    public static long getTimestamp(String year, String month, String day, String hour, String mins) {
		
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT"));
		c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
		c.set(Calendar.YEAR, Integer.valueOf(year));
		c.set(Calendar.MONTH, Integer.valueOf(month) - 1);
		
		c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hour));
		c.set(Calendar.MINUTE, Integer.valueOf(mins));
		c.set(Calendar.SECOND, 0);
		
		long baseTime = c.getTimeInMillis();
		
		return baseTime;
	}
}

















